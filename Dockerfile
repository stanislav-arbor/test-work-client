FROM node:8-alpine

COPY package.json package-lock.json ./

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

RUN npm i && mkdir /itemer && cp -R ./node_modules ./itemer

WORKDIR /itemer

COPY . .
