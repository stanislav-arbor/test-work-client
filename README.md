# Angular 5 client
## Introduction
Simple items list with filter powered by angular 5


## Install
First we need to install dependencies
```
npm install
```
After all done we can run application
```
npm start
```
After webpack compile aplication you can visit `localhost:4200`

Endpoints:

- Angular: `localhost:4200`
- Rails: `localhost:3000`
