import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ItemsListComponent } from './components/items-list/items-list.component';
import { ItemComponent } from './components/item/item.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HomeComponent } from './components/home/home.component';
import { CategoryFilterComponent } from './components/category-filter/category-filter.component';
import { ItemCardComponent } from './components/item-card/item-card.component';

import { ItemService } from './services/item.service';
import { CategoryService } from './services/category.service';

import { CategoryFilterPipe } from './pipes/category-filter.pipe';

const appRoutes: Routes = [
  { path: 'items', component: ItemsListComponent },
  { path: 'items/:slug', component: ItemComponent },
  { path: '', component: HomeComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ItemsListComponent,
    ItemComponent,
    PageNotFoundComponent,
    HomeComponent,
    CategoryFilterComponent,
    CategoryFilterPipe,
    ItemCardComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule
  ],
  providers: [
    ItemService,
    CategoryService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
