import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';

import { Item } from '../models/Item';

@Injectable()
export class ItemService {
  private baseUrl = `${environment.apiEndpoint}/items`

  constructor(private http: HttpClient) { }

  loadItems(): Observable<Item[]> {
    return this.http.get<Item[]>(this.baseUrl)
  }

  loadItem(slug): Observable<Item> {
    const singleItemUrl = `${this.baseUrl}/${slug}`
    return this.http.get<Item>(singleItemUrl)
  }
}
