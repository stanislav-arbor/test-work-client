import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Item } from '../../models/Item';
import { ItemService } from '../../services/item.service';

@Component({
  templateUrl: './item.component.html'
})

export class ItemComponent implements OnInit {
  item: Item = new Item;

  constructor(
    private itemsService: ItemService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const itemId = this.route.snapshot.paramMap.get('slug');
    this.itemsService.loadItem(itemId)
      .subscribe(item => this.item = item)
  }

  categoriesNames(): string {
    if (this.item.categories) {
      return this.item.categories.map(category => category.name).join(', ')
    }
  }
}
