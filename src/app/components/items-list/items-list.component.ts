import { Component, OnInit } from '@angular/core';

import { Item } from '../../models/Item';
import { ItemService } from '../../services/item.service';

@Component({
  templateUrl: './items-list.component.html'
})

export class ItemsListComponent implements OnInit {
  colClasses: string[] = [
    'col-xs-12',
    'col-sm-6',
    'col-md-4',
    'col-lg-3',
    'pb-3',
    'pt-3'
  ];
  items: Item[] = [];
  categoryIdFilter: number;
  paginateConfigs: { itemsPerPage: number, currentPage: number } = {
    itemsPerPage: 10,
    currentPage: 1
  }

  constructor(private itemsService: ItemService) {}

  ngOnInit() {
    this.itemsService.loadItems().subscribe(items => this.items = items)
  }

  pageChanged($event) {
    this.paginateConfigs.currentPage = $event
  }

  filterItemsByCategoryId(id) {
    this.categoryIdFilter = id
  }

  changeItemsPerPageNumber(itemsPerPageNumber) {
    if (itemsPerPageNumber > 0) {
      this.paginateConfigs.itemsPerPage = itemsPerPageNumber
    }
  }
}
