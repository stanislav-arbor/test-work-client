import { Component, Input } from '@angular/core';

import { Item } from '../../models/Item';

@Component({
  selector: 'item-card',
  templateUrl: './item-card.component.html',
})

export class ItemCardComponent {
  @Input() item: Item;
}
