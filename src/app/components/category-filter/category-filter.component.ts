import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { Category } from '../../models/category';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'category-filter',
  templateUrl: './category-filter.component.html',
})

export class CategoryFilterComponent implements OnInit {
  @Output() filterItemsByCategoryId = new EventEmitter();
  @Output() changeItemsPerPageNumber = new EventEmitter();
  @Input() itemsPerPageNumber: number;

  categories: Category[] = [];

  constructor(private categoryService: CategoryService) {}

  ngOnInit() {
    this.categoryService.loadCategories()
      .subscribe(categories => this.categories = categories)
  }

  onChangedCategoryId(event:any) {
    this.filterItemsByCategoryId.emit(parseInt(event.target.value, 10));
  }

  onChangedPerPageNumber() {
    this.changeItemsPerPageNumber.emit(this.itemsPerPageNumber);
  }
}
