import { Category } from './category'

export class Item {
  id: number;
  name: string;
  price: number;
  categories: Category[];
}
