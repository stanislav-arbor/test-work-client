import { Pipe, PipeTransform } from '@angular/core';

import { Item } from '../models/Item';

@Pipe({
  name: 'categoryFilter'
})

export class CategoryFilterPipe implements PipeTransform {
  transform(items: any[], categoryFilterId: number): any {
    if (this.isFilterEmpty(categoryFilterId)) return items

    return items.filter(item => this.isHasCategory(item, categoryFilterId))
  }

  private isFilterEmpty(categoryId: number): boolean {
    return typeof categoryId === 'undefined' || isNaN(categoryId)
  }

  private isHasCategory(item: Item, categoryId: number): boolean {
    return item.categories.findIndex(category => category.id == categoryId) !== -1
  }
}
